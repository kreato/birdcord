// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const path = require('path')

const urls = [
    "https://discord.com/login"
]


app.on(
    "window-all-closed",
    () => process.platform !== "darwin" && app.quit() // "darwin" targets macOS only.
);

const createWindow = () =>{
    win = new BrowserWindow({
        center: true,
        icon: 'discord.ico',
        resizable: true,
        webPreferences:{
            nodeIntegration: false,
            show: false
        }
    });
 //   win.maximize();
 //   win.webContents.openDevTools();
    //win.webContents.
    console.log(urls[0]);

    win.loadURL(urls[0]);
    // win.loadURL(url.format({
    //     pathname: path.join(__dirname,"index.html"),
    //     protocol: 'file',
    //     slashes: true
    // }));
    win.once('ready-to-show',()=>{
        win.show()
    });

    win.on('closed',()=>{
        win = null;
    });
}

app.on('ready', createWindow);
